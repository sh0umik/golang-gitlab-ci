// Simple golang App
package main

import (
	"fmt"
	"gitlab.com/sh0umik/golang-gitlab-ci/connectors"
	"gitlab.com/sh0umik/golang-gitlab-ci/models"
)

type Profile struct {
	Name string `json:"name"`
}
func main() {

	cfg := models.Config{
		DBHost:        "fahim74-couchbase-for-ci",
		BucketName:    "data",
		NoSqlPassword: "password",
		NoSqlUser:     "admin",
	}

	db, err := connectors.GetDBConnection(&cfg)
	if err != nil {
		panic(err)
	}

	// Test Insert

	_, err = db.Insert("db::key", &Profile{Name: "Fahim"}, 0)
	if err != nil {
		fmt.Println(err.Error())
	}

	// Test Get

	var profile Profile
	_, err = db.Get("db::key", &profile)
	if err != nil {
		fmt.Println(err.Error())
	}

	fmt.Println(profile)

}

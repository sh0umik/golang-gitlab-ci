FROM alpine

WORKDIR /go/src/gitlab.com/sh0umik/golang-gitlab-ci
COPY golang-gitlab-ci /go/src/gitlab.com/sh0umik/golang-gitlab-ci

# RUN apk add --update --no-cache ca-certificates

ENV GOPATH /go

ENTRYPOINT /go/src/gitlab.com/sh0umik/golang-gitlab-ci/golang-gitlab-ci

# Service listens on port 8080.
EXPOSE 8080
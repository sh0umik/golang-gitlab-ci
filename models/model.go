package models

type Config struct {
	// Database Information
	NoSqlUser      string
	NoSqlPassword  string
	DBHost         string
	DBPort         string
	BucketName     string
	BucketPassword string
}


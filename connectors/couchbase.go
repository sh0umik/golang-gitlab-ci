package connectors

import (
	"errors"
	"fmt"
	"github.com/getsentry/raven-go"
	"gitlab.com/sh0umik/golang-gitlab-ci/models"
	"gopkg.in/couchbase/gocb.v1"
)

type DBConnection struct {
	*gocb.Bucket
}

func GetDBConnection(cfg *models.Config) (*DBConnection, error) {

	cluster, err := gocb.Connect("couchbase://" + cfg.DBHost)
	if err != nil {
		return nil, errorMessage(err, "Connect Init")
	}

	auth := &gocb.PasswordAuthenticator{
		Username: cfg.NoSqlUser,
		Password: cfg.NoSqlPassword,
	}

	err = cluster.Authenticate(auth)

	if err != nil {
		return nil, errorMessage(err, "Connect Auth")
	}

	var bucket *gocb.Bucket

	// It takes some time to connect to couchbase bucket thats why
	for {
		bucket, err = cluster.OpenBucket(cfg.BucketName, "")
		if err == nil {
			break
		}
		msg := fmt.Sprintf("DBConnection :: Couchbase :: Cluster > Open Bucket :: Error : %s :: trying again ...", err.Error())
		fmt.Println(msg)
	}

	fmt.Println("\n\nConnected to NOSQL DB")

	return &DBConnection{bucket}, nil
}

// #todo better exception handling and logging
func errorMessage(err error, context string) error {
	msg := fmt.Sprintf("err", context, err.Error())
	fmt.Println(msg)
	// Send to Sentry
	raven.CaptureError(err, map[string]string{"err": msg})
	return errors.New(msg)
}
